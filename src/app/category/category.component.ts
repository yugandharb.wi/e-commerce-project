import { Component, OnInit } from "@angular/core";
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material';
interface FoodNode {
  name: string;
  children?: FoodNode[];
}

const TREE_DATA: FoodNode[] = [
  {
    name: "Electronics",
    children: [{ name: "laptoop" }, { name: "cell phones" }, { name: "home cinema" }]
  },
  {
    name: "Gifts",
    children: [{ name: "Rings" }, { name: "Earrings" },]
  },
 
];

/**
 * @title Tree with nested nodes
 */
@Component({
  selector: "app-category",
  templateUrl: "./category.component.html",
  styleUrls: ["./category.component.css"]
})
export class CategoryComponent  {
  treeControl = new NestedTreeControl<FoodNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<FoodNode>();
  constructor() {
    this.dataSource.data = TREE_DATA;
  }

  hasChild = (_: number, node: FoodNode) =>
    !!node.children && node.children.length > 0;
}
