import { BrowserModule } from "@angular/platform-browser";
import { NgModule, Component } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";
import { JoinComponent } from "./join/join.component";
import { BarreComponent } from "./barre/barre.component";
import { SlideComponent } from "./slide/slide.component";
import { CategoryComponent } from "./category/category.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  
  MatDialogModule,
  MatButtonModule,
  MatInputModule,
  MatSelectModule,
  MatCheckboxModule,
  MatRadioButton,
  MatRadioModule,
  MatInput,
  MatAutocompleteModule,
  MatMenuModule,
  MatTree,
  MatTreeModule,
 
} from "@angular/material";
import { MatFormFieldModule } from "@angular/material/form-field";
import { AccountComponent } from './cart/cart.component';
import { HomeComponent } from './home/home.component';
import { ProductComponent } from './product/product.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { MatIconModule } from '@angular/material';
import { PayerComponent } from './payer/payer.component';
import { FooterComponent } from './footer/footer.component';

const appRoutes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "home", component: HomeComponent},
  { path: "signup", component: JoinComponent },
  { path: "home/mycart", component: AccountComponent },
  { path: "home/newproduct", component: ProductComponent },


  
  
];
@NgModule({
  declarations: [
    AppComponent,
    JoinComponent,
    AccountComponent,
    BarreComponent,
    SlideComponent,
    CategoryComponent,
    HomeComponent,
    ProductComponent,
    PayerComponent,
    FooterComponent
  ],

  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatIconModule,
    MatCheckboxModule,
    MatRadioModule,
    MatInputModule,
    MatAutocompleteModule,
    MatMenuModule,
    AngularFontAwesomeModule,
    MatIconModule,
    MatTreeModule
  ],
  entryComponents: [JoinComponent,PayerComponent],
  exports: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
