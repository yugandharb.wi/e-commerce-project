import { Component, OnInit } from "@angular/core";
import { JoinComponent } from '../join/join.component';
import { MatDialog } from '@angular/material';
import { AccountComponent } from '../cart/cart.component';

@Component({
  selector: "app-barre",
  templateUrl: "./barre.component.html",
  styleUrls: ["./barre.component.css"]
})
export class BarreComponent  {
  constructor(public dialog: MatDialog , public dialog1:MatDialog) {}
  
  
  openDialog(): void {
    const dialogRef = this.dialog.open(JoinComponent, {
    });
    

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
    
  }
  openDialog1(): void {
    const dialogRef = this.dialog.open(AccountComponent, {
    });
    

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
    
  }
  
  
  
}
