import { Component, OnInit } from "@angular/core";
import { FormControl, Validators, FormGroup } from "@angular/forms";

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialog, MatDialogRef } from '@angular/material';
import { PayerComponent } from '../payer/payer.component';
import { AppModule } from '../app.module';


@Component({
  selector: "app-join",
  templateUrl: "./join.component.html",
  styleUrls: ["./join.component.css"]
})
export class JoinComponent {
  email = new FormControl("", [Validators.required, Validators.email]);
  hide = true;
  options: FormGroup;
  constructor(public dialog: MatDialog,  private dialogRef: MatDialogRef<AppModule>,
    ) {}
  getErrorMessage() {
    return this.email.hasError("required")
      ? "You must enter a value"
      : this.email.hasError("email")
      ? "Not a valid email"
      : "";
  }
  onNoClick(): void {
    this.dialogRef.close();
   
 }}
